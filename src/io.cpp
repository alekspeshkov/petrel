#include "io.hpp"

namespace io {

istream& fail(istream& in) {
    in.setstate(std::ios::failbit);
    return in;
}

istream& fail_char(istream& in) {
    in.unget();
    return fail(in);
}

istream& fail_pos(istream& in, std::streampos here) {
    in.clear();
    in.seekg(here);
    return fail(in);
}

istream& fail_rewind(istream& in) {
    return fail_pos(in, 0);
}

/// Read the input stream and try to match it with the given token parameter.
/// The comparision is case insensitive and all sequential space symbols are equal to single space.
/// @retval true: stream matches the given token, stream position is set forward past the matched token
/// @retval false: failed to match stream with the given token, stream state reset back before call
bool next(istream& in, czstring token) {
    if (token == nullptr) { token = ""; }

    auto state = in.rdstate();
    auto before = in.tellg();

    using std::isspace;
    do {
        while ( isspace(*token) ) { ++token; }
        in >> std::ws;

        while ( !isspace(*token) && std::tolower(*token) == std::tolower(in.peek()) ) {
            ++token; in.ignore();
        }
    } while ( isspace(*token) && isspace(in.peek()) );

    if ( *token == '\0' && (isspace(in.peek()) || in.eof()) ) {
        return true;
    }

    in.seekg(before);
    in.clear(state);
    return false;
}

bool nextNothing(istream& in) {
    in >> std::ws;
    return in.eof();
}

ostream& app_version(ostream& out) {

#ifndef NDEBUG
    out << " DEBUG";
#endif

#ifdef GIT_DATE
    out << ' ' << GIT_DATE;
#else
    char_type year[] {__DATE__[7], __DATE__[8], __DATE__[9], __DATE__[10], '\0'};

    char_type month[] {
        (__DATE__[0] == 'O' && __DATE__[1] == 'c' && __DATE__[2] == 't') ? '1' :
        (__DATE__[0] == 'N' && __DATE__[1] == 'o' && __DATE__[2] == 'v') ? '1' :
        (__DATE__[0] == 'D' && __DATE__[1] == 'e' && __DATE__[2] == 'c') ? '1' : '0',

        (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n') ? '1' :
        (__DATE__[0] == 'F' && __DATE__[1] == 'e' && __DATE__[2] == 'b') ? '2' :
        (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r') ? '3' :
        (__DATE__[0] == 'A' && __DATE__[1] == 'p' && __DATE__[2] == 'r') ? '4' :
        (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y') ? '5' :
        (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n') ? '6' :
        (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l') ? '7' :
        (__DATE__[0] == 'A' && __DATE__[1] == 'u' && __DATE__[2] == 'g') ? '8' :
        (__DATE__[0] == 'S' && __DATE__[1] == 'e' && __DATE__[2] == 'p') ? '9' :
        (__DATE__[0] == 'O' && __DATE__[1] == 'c' && __DATE__[2] == 't') ? '0' :
        (__DATE__[0] == 'N' && __DATE__[1] == 'o' && __DATE__[2] == 'v') ? '1' :
        (__DATE__[0] == 'D' && __DATE__[1] == 'e' && __DATE__[2] == 'c') ? '2' : '0',

        '\0'
    };

    char_type day[] {((__DATE__[4] == ' ') ? '0' : __DATE__[4]), __DATE__[5], '\0'};

    out << ' ' << year << '-' << month << '-' << day;
#endif

#ifdef GIT_ORIGIN
    out << ' ' << GIT_ORIGIN;
#else
    out << " https://bitbucket.org/alekspeshkov/petrel/src/";
#endif

#ifdef GIT_HASH
    out << ' ' << GIT_HASH;
#endif

    return out;
}

ostream& option_version(ostream& out) {
    return out
        << "petrel" << app_version << '\n'
        << "(c) Aleks Peshkov (aleks.peshkov@gmail.com)\n"
    ;
}

ostream& option_help(ostream& out) {
    return out
        << "    Petrel chess engine. The UCI protocol compatible.\n\n"
        << "      -h, --help        display this help\n"
        << "      -v, --version     display version information\n"
    ;
}

ostream& option_invalid(ostream& err) {
    return err << "petrel: unkown option\n";
}

ostream& uci_error(ostream& err, istream& context) {
    return err << "parsing error: " << context.rdbuf() << '\n';
}

} //end of namespace io
