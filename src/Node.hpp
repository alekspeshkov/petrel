#ifndef NODE_HPP
#define NODE_HPP

#include "Score.hpp"
#include "PositionMoves.hpp"

enum class NodeControl : bool {
    Continue = false,
    Abort = true
};

#define RETURN_IF_ABORT(control) { NodeControl c{control}; if (c != NodeControl::Continue) { return c; } } ((void)0)

class SearchControl;

class Node : public PositionMoves {
protected:
    Node& parent; //virtual

public:
    SearchControl& control;
    ply_t draft; //remaining distance to leaves

protected:
    Node (Node& n, ply_t r = 1);
    Node (const PositionMoves& p, SearchControl& c, ply_t d) : PositionMoves{p}, parent{*this}, control{c}, draft{d} {}

public:
    virtual ~Node() = default;
    virtual NodeControl beforeVisit();
    virtual NodeControl visit(Square, Square) { return NodeControl::Continue; }
    virtual NodeControl visitChildren();
};

#endif
