#ifndef PI_SINGLE_HPP
#define PI_SINGLE_HPP

#include "bitops128.hpp"
#include "typedefs.hpp"

class PiSingle {
    typedef i128_t _t;

    Pi::arrayOf<u8x16_t> v;

public:
    constexpr PiSingle () : v {{
        {{0xff,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}},
        {{0,0xff,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}},
        {{0,0,0xff,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}},
        {{0,0,0,0xff, 0,0,0,0, 0,0,0,0, 0,0,0,0}},

        {{0,0,0,0, 0xff,0,0,0, 0,0,0,0, 0,0,0,0}},
        {{0,0,0,0, 0,0xff,0,0, 0,0,0,0, 0,0,0,0}},
        {{0,0,0,0, 0,0,0xff,0, 0,0,0,0, 0,0,0,0}},
        {{0,0,0,0, 0,0,0,0xff, 0,0,0,0, 0,0,0,0}},

        {{0,0,0,0, 0,0,0,0, 0xff,0,0,0, 0,0,0,0}},
        {{0,0,0,0, 0,0,0,0, 0,0xff,0,0, 0,0,0,0}},
        {{0,0,0,0, 0,0,0,0, 0,0,0xff,0, 0,0,0,0}},
        {{0,0,0,0, 0,0,0,0, 0,0,0,0xff, 0,0,0,0}},

        {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0xff,0,0,0}},
        {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0xff,0,0}},
        {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0xff,0}},
        {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0xff}},
    }}
    {}

    constexpr const _t& operator[] (Pi pi) const { return v[pi].i128; }
};

extern const PiSingle piSingle;

#endif
